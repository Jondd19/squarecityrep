﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    // Start is called before the first frame update
    
    public Text title;
    public List<InventoryItem> itemList = new List<InventoryItem>();
    public InventoryItem[] itemArray;
    public InventoryListControl ILC;
    public int tab;
    public bool shop;
    public bool buy;
    public List<InventoryListSlot> equippedItems = new List<InventoryListSlot>();
    public Inventory playerInventory;
    public List<InventoryItem> shopItemList = new List<InventoryItem>();
    public GameObject player;

    public enum Tab
    {
        All,
        Weapons,
        Armor,
        Consumables,
        Materials
    };
    void Awake()
    {


        
            
        



        tab = 0;
        if (shop == false)
        {
           /* AddItem(1, 1, 10, "Iron Sword&Shield");
            AddItem(1, 1, 10, "Iron Rapier");
            AddItem(1, 1, 10, "Iron Sword&Shield");
            AddItem(2, 1, 10, "Iron Armor");
            AddItem(2, 3, 10, "Steel Armor");
            AddItem(3, 10, 10, "Arrow");
            AddItem(3, 3, 10, "Health Potion");
            AddItem(3, 3, 10, "Health Potion");
            AddItem(4, 1, 10, "Iron");
            AddItem(4, 1, 10, "Coal");
            AddItem(4, 1, 10, "Iron");*/
        }
        else
        {
            AddItem(1, 1, 10, "Iron Rapier");
        }
        AllSwap();
        

    }

     void Start()
    {
        player = GameObject.Find("Player");
        shopItemList = this.itemList;
        playerInventory = GameObject.Find("Inventory").GetComponent<Inventory>();
    }
    // Update is called once per frame
    void Update()
    {
     //  if (Input.GetKeyDown(KeyCode.Z))
      //  {
            equippedItemsUpdate();
        //}


         if (Input.GetKeyDown(KeyCode.Z))
          {
        foreach (InventoryListSlot item in equippedItems)
            {
                print(item.myText.text);
            }
        }

    }


    public void equippedItemsUpdate()
    {
        foreach (InventoryItem item in itemList)
        {
            int itemcounter = 0;  
            foreach (InventoryListSlot eqItem in equippedItems)
            {
                
                if (eqItem.myText.text == item.itemName)
                {
                    itemcounter++;
                }
               
            }
            if (itemcounter > 0)
            {
                item.equipped = true;
               // print("yes");
            }
            else
            {
                item.equipped = false;
               // print("no");
            }
        }
       // InventoryUpdate();
    }
    public void AddItem(int TabValue, int InvCount, int price, string ItemName)
    {
        InventoryItem invItem = new InventoryItem();
        invItem.tabValue = TabValue;
        invItem.invCount = InvCount;
        invItem.itemName = ItemName;
        invItem.price = price;
        List<InventoryItem> tempItemList = new List<InventoryItem>();
        tempItemList = itemList;
        bool additem = true;

        if (itemList.Count == 0)
        {
            itemList.Add(invItem);
            itemArray = itemList.ToArray();
        }
        else
        {
            itemArray = itemList.ToArray();


            for (int i = 0; i < itemArray.Length; i++)
            {
                if (itemArray[i].itemName == ItemName)
                {
                    itemArray[i].invCount += InvCount;
                    additem = false;
                }
                
            }

            if (additem)
            {
                tempItemList.Add(invItem);
            }

            
        }

        itemList = tempItemList;
        itemArray = itemList.ToArray();

    }

    

    public void RemoveItem(int InvCount, string ItemName)
    {
        
        List<InventoryItem> tempItemList = new List<InventoryItem>();
        tempItemList = itemList;
        InventoryItem itemToRemove = new InventoryItem();
        bool removeitem = false;

            foreach(InventoryItem item in tempItemList)
        {
            if (item.itemName == ItemName)
            {
                item.invCount -= InvCount;

                if (item.invCount <= 0)
                {
                    removeitem = true;
                    itemToRemove = item;
                    
                }
            }
        }


        if (removeitem)
        {

            tempItemList.Remove(itemToRemove);
        }

        itemList = tempItemList;
        itemArray = itemList.ToArray();
        InventoryUpdate();
    }

   


    public void InventoryUpdate()
    {
        if (tab == 0)
        {
            AllSwap();
        }
        if (tab == 1)
        {
            WeaponSwap();
        }
        if (tab == 2)
        {
            ArmorSwap();
        }
        if (tab == 3)
        {
            ConsumableSwap();
        }
        if (tab == 4)
        {
            MaterialsSwap();
        }
        if (buy)
        {
            this.itemList = shopItemList;
            itemArray = itemList.ToArray();
        }
        else
        {
            this.itemList = playerInventory.itemList;
            itemArray = itemList.ToArray();
        }
    }
    public void AllSwap()
    {
        tab = 0;
        ILC.ClearList();
        title.text = "All Items";
        if (ILC.buttonsGenerated == false && itemArray != null)
        {
            ILC.buttonsGenerated = true;
            //print("test");
            for (int i = 0; i < itemArray.Length; i++)
            {
                if (!shop && itemArray[i].invCount > 0)
                {
                    ILC.GenerateButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].equipped,itemArray[i].tabValue);
                }
                else if(itemArray[i].invCount > 0)
                {
                    ILC.GenerateShopButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].price);
                }
            }
        }
    }

   public void WeaponSwap()
    {
        tab = 1;
        ILC.ClearList();
        title.text = "Weapons";
        if (ILC.buttonsGenerated == false)
        {

            for (int i = 0; i < itemArray.Length; i++)
            {
                if (itemArray[i].tabValue == 1)
                {
                    if (!shop && itemArray[i].invCount > 0)
                    {
                        ILC.GenerateButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].equipped, itemArray[i].tabValue);
                    }
                    else if(itemArray[i].invCount > 0)
                    {
                        ILC.GenerateShopButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].price);
                    }
                }
            }
        }
    }
   public void ArmorSwap()
    {
        tab = 2;
        ILC.ClearList();
        title.text = "Armor";
        if (ILC.buttonsGenerated == false)
        {
            for (int i = 0; i < itemArray.Length; i++)
            {
                if (itemArray[i].tabValue == 2)
                {
                    if (!shop && itemArray[i].invCount > 0)
                    {
                        ILC.GenerateButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].equipped, itemArray[i].tabValue);
                    }
                    else if (itemArray[i].invCount > 0)
                    {
                        ILC.GenerateShopButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].price);
                    }
                }
            }
        }

    }
   public void ConsumableSwap()
    {
        tab = 3;
        ILC.ClearList();
        title.text = "Consumables";
        if (ILC.buttonsGenerated == false)
        {
            for (int i = 0; i < itemArray.Length; i++)
            {
                if (itemArray[i].tabValue == 3)
                {
                    if (!shop && itemArray[i].invCount > 0)
                    {
                        ILC.GenerateButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].equipped, itemArray[i].tabValue);
                    }
                    else if (itemArray[i].invCount > 0)
                    {
                        ILC.GenerateShopButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].price);
                    }
                }
            }
        }
    }
    public void MaterialsSwap()
    {
        tab = 4;
        ILC.ClearList();
        title.text = "Materials";
        if (ILC.buttonsGenerated == false)
        {
            for (int i = 0; i < itemArray.Length; i++)
            {
                if (itemArray[i].tabValue == 4)
                {
                    if (!shop && itemArray[i].invCount > 0)
                    {
                        ILC.GenerateButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].equipped, itemArray[i].tabValue);
                    }
                    else if (itemArray[i].invCount > 0)
                    {
                        ILC.GenerateShopButton(itemArray[i].itemName, itemArray[i].invCount, itemArray[i].price);
                    }
                }
            }
        }
    }
    public void BuySwap()
    {
        buy = true;
        this.itemList = shopItemList;
        itemArray = itemList.ToArray();
        AllSwap();

    }
    public void SellSwap()
    {
        buy = false;
        this.itemList = playerInventory.itemList;
        itemArray = itemList.ToArray();
        AllSwap();

    }
}
