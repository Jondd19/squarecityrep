﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryListSlot : MonoBehaviour
{
    [SerializeField]
    public Text myText;
    public Text numberText;
    public Text equippedText;
    public GameObject Player;
    public GameManager gameManager;
    public bool equipped;
    public Inventory inventory;
    public Inventory shop;
    InventoryListSlot toSwap = null;
    public int itemClassId;
    public void SetText(string textString, string NumberText)
    {
        myText.text = textString;
        numberText.text = NumberText;
        equippedText.enabled = false;
        equipped = false;
    }

    public void Start()
    {
        
        Player = GameObject.Find("Player");
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (gameManager.isShopping == true)
        {
            shop = GameObject.Find("Shop").GetComponent<Inventory>();
        }
    }
    public void OnClick()
    {
        

        if (shop != null && shop.shop == true)
        {
            foreach (InventoryItem item in shop.itemList)
            {
                if (shop.buy)
                {
                    if(myText.text == item.itemName && Player.GetComponent<Character>().gold>=item.price)
                    {
                        Player.GetComponent<Character>().gold -= item.price;
                        shop.RemoveItem(1, item.itemName);
                        inventory.AddItem(item.tabValue, 1, item.price, item.itemName);
                    }
                }
                else
                {
                    if (myText.text == item.itemName)
                    {
                        Player.GetComponent<Character>().gold += item.price;
                        shop.RemoveItem(1, item.itemName);
                        shop.BuySwap();
                        shop.AddItem(item.tabValue, 1, item.price, item.itemName);
                        shop.SellSwap();

                    }
                }
            }
        }
        else
        {



            if (myText.text == "Copper Armor")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        if (inventory.equippedItems.Count != 0)
                        {


                            foreach (InventoryListSlot item in inventory.equippedItems)
                            {
                                if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                                {
                                    toSwap = item;
                                }

                            }
                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 0;
                    }
                    else
                    {
                        //Player.GetComponent<Character>()
                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 0;
                    }

                }
                else
                {
                    //Player.GetComponent<Character>()
                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;

                }

            }
            if (myText.text == "Bronze Armor")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        if (inventory.equippedItems.Count != 0)
                        {


                            foreach (InventoryListSlot item in inventory.equippedItems)
                            {
                                if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                                {
                                    toSwap = item;
                                }

                            }
                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 1;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 1;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;

                }

            }
            if (myText.text == "Iron Armor")
            {

                if (!equipped)
                {

                    if (inventory.equippedItems != null)
                    {
                        if (inventory.equippedItems.Count != 0)
                        {


                            foreach (InventoryListSlot item in inventory.equippedItems)
                            {
                                if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                                {
                                   // print("Test");
                                    toSwap = item;
                                    //print(toSwap.myText.text);
                                }

                            }
                        }

                    }
                    if (toSwap == null)
                    {
                        //print("NullSwap");
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 2;

                    }
                    else
                    {
                        //print("NotNullSwap");
                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        equippedText.enabled = true;
                        equipped = true;
                        toSwap = null;
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 2;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;

                }

            }
            if (myText.text == "Steel Armor")
            {
                if (!equipped)
                {

                    if (inventory.equippedItems != null)
                    {

                        if (inventory.equippedItems.Count != 0)
                        {


                            foreach (InventoryListSlot item in inventory.equippedItems)
                            {
                                if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                                {
                                    toSwap = item;
                                }

                            }
                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 3;

                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 3;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;
                }

            }
            if (myText.text == "Lightsteel Armor")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        if (inventory.equippedItems.Count != 0)
                        {


                            foreach (InventoryListSlot item in inventory.equippedItems)
                            {
                                if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                                {
                                    toSwap = item;
                                }

                            }
                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 4;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 4;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;
                }

            }
            if (myText.text == "Forcesteel Armor")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        if (inventory.equippedItems.Count != 0)
                        {


                            foreach (InventoryListSlot item in inventory.equippedItems)
                            {
                                if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                                {
                                    toSwap = item;
                                }

                            }
                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 5;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 5;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;
                }

            }
            if (myText.text == "Bluesteel Armor")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 6;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = true;
                        Player.GetComponent<Character>().armorvalue = 6;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;
                }

            }
            if (myText.text == "Copper Rapier")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 0;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 0;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Bronze Rapier")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 1;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 1;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Iron Rapier")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 2;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 2;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Steel Rapier")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 3;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 3;
                    }

                }
                else
                {
                    //Player.GetComponent<Character>()
                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Lightsteel Rapier")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 4;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 4;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Forcesteel Rapier")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 5;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 5;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Bluesteel Rapier")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 6;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 0;
                        Player.GetComponent<Character>().weaponmatvalue = 6;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Copper Sword&Shield")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 0;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 0;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Bronze Sword&Shield")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 1;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 1;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Iron Sword&Shield")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 2;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 2;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Steel Sword&Shield")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 3;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 3;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Lightsteel Sword&Shield")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 4;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 4;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Forcesteel Sword&Shield")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 5;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 5;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Bluesteel Sword&Shield")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 6;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 1;
                        Player.GetComponent<Character>().weaponmatvalue = 6;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;

                }

            }
            if (myText.text == "Copper Bow")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 0;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 0;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;
                }

            }
            if (myText.text == "Bronze Bow")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 1;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 1;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 0;
                }

            }
            if (myText.text == "Iron Bow")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 2;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 2;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;
                }

            }
            if (myText.text == "Steel Bow")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 3;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 3;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;
                }

            }
            if (myText.text == "Lightsteel Bow")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 4;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 4;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;
                }

            }
            if (myText.text == "Forcesteel Bow")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 5;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 5;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;
                }

            }
            if (myText.text == "Bluesteel Bow")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Sword&Shield") || item.myText.text.Contains("Rapier") || item.myText.text.Contains("Bow"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 6;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().weaponvalue = 2;
                        Player.GetComponent<Character>().weaponmatvalue = 6;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().weaponvalue = 3;
                }

            }
            if (myText.text == "Silk Robe")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = false;
                        Player.GetComponent<Character>().clothesvalue = 1;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = false;
                        Player.GetComponent<Character>().clothesvalue = 1;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;
                }

            }
            if (myText.text == "Lightsilk Robe")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = false;
                        Player.GetComponent<Character>().clothesvalue = 2;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = false;
                        Player.GetComponent<Character>().clothesvalue = 2;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;
                }

            }
            if (myText.text == "Forcesilk Robe")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = false;
                        Player.GetComponent<Character>().clothesvalue = 3;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = false;
                        Player.GetComponent<Character>().clothesvalue = 3;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;
                }

            }
            if (myText.text == "Bluesilk Robe")
            {
                if (!equipped)
                {
                    if (inventory.equippedItems != null)
                    {
                        foreach (InventoryListSlot item in inventory.equippedItems)
                        {
                            if (item.myText.text.Contains("Robe") || item.myText.text.Contains("Armor"))
                            {
                                toSwap = item;
                            }

                        }

                    }
                    if (toSwap == null)
                    {
                        equippedText.enabled = true;
                        equipped = true;

                        inventory.equippedItems.Add(this);
                        Player.GetComponent<Character>().armored = false;
                        Player.GetComponent<Character>().clothesvalue = 4;
                    }
                    else
                    {

                        inventory.equippedItems[inventory.equippedItems.FindIndex(ind => ind.Equals(toSwap))] = this;
                        toSwap.equipped = false;
                        toSwap.equippedText.enabled = false;
                        toSwap = null;
                        equippedText.enabled = true;
                        equipped = true;
                        Player.GetComponent<Character>().armored = false;
                        Player.GetComponent<Character>().clothesvalue = 4;
                    }

                }
                else
                {

                    equippedText.enabled = false;
                    equipped = false;
                    inventory.equippedItems.Remove(this);
                    Player.GetComponent<Character>().armored = false;
                    Player.GetComponent<Character>().clothesvalue = 0;
                }

            }

            if (myText.text == "Health Potion")
            {
                inventory.RemoveItem(1, "Health Potion");
                Player.GetComponent<Character>().currenthp += 5;

            }
        }

            Player.GetComponent<Character>().Statupdate();
        if (shop != null)
        {
            shop.InventoryUpdate();
        }
    }
}
