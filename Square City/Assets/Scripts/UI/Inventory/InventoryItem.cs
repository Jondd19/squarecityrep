﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem 
{
    public int tabValue;

    public string itemName;

    public int invCount;

    public int price;

    public bool equipped;

    /*public InventoryItem(int TabValue, int InvCount, string ItemName)
    {
        tabValue = TabValue;
        itemName = ItemName;
        invCount = InvCount;
        
    }*/
}
