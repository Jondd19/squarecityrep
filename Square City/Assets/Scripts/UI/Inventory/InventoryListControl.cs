﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InventoryListControl : MonoBehaviour
{


    [SerializeField]
    private GameObject inventoryTemplate;
    public List<GameObject> buttonList = new List<GameObject>();
    List<GameObject> addToList = new List<GameObject>();
    public bool buttonsGenerated;
    public Inventory inventory;
    private void Start()
    {
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
    }


    
    public void GenerateButton( string ButtonName, int ButtonCount, bool equipped, int id)
    {




        if (!equipped)
        {
            GameObject slot = Instantiate(inventoryTemplate) as GameObject;
            buttonList.Add(slot);
            slot.GetComponent<InventoryListSlot>().itemClassId = id;
            slot.SetActive(true);
            slot.GetComponent<InventoryListSlot>().SetText(ButtonName, ("" + ButtonCount));
            slot.transform.SetParent(inventoryTemplate.transform.parent, false);
        }
       

        
    }

    public void GenerateShopButton(string ButtonName, int ButtonCount, int Price)
    {



        GameObject slot = Instantiate(inventoryTemplate) as GameObject;
        buttonList.Add(slot);

        slot.SetActive(true);
        slot.GetComponent<InventoryListSlot>().SetText(ButtonName, ("x" + ButtonCount+"/"+Price+" gp"));

        slot.transform.SetParent(inventoryTemplate.transform.parent, false);


    }

    public void ClearList()
    {
        
        
        if (buttonList.Count > 0)
        {
            foreach (GameObject button in buttonList)
            {
                if (button != null)
                {
                    if (button.GetComponent<InventoryListSlot>().equipped != true)
                    {
                        Destroy(button);
                    }
                    else
                    {
                        addToList.Add(button);
                    }
                }
            }
        }
        buttonList.Clear();

        foreach (GameObject addedButton in addToList)
        {
            buttonList.Add(addedButton);
        }

        buttonsGenerated = false;

        foreach(GameObject equippedButtons in buttonList)
        {
            equippedButtons.SetActive(false);
            if (inventory.tab == 0)
            {
                equippedButtons.SetActive(true);
            }
            if (inventory.tab == 1 && equippedButtons.GetComponent<InventoryListSlot>().itemClassId == 1)
            {
                equippedButtons.SetActive(true);
            }
            if (inventory.tab == 2 && equippedButtons.GetComponent<InventoryListSlot>().itemClassId == 2)
            {
                equippedButtons.SetActive(true);
            }
            if (inventory.tab == 3 && equippedButtons.GetComponent<InventoryListSlot>().itemClassId == 2)
            {
                equippedButtons.SetActive(true);

            }
            if (inventory.tab == 4 && equippedButtons.GetComponent<InventoryListSlot>().itemClassId == 3)
            {
                equippedButtons.SetActive(true);

            }
        }
    }
}
