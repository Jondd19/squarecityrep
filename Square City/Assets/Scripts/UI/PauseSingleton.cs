﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseSingleton : MonoBehaviour
{
   static PauseSingleton instance;
    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }
}
