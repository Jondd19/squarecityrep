﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiSingleton : MonoBehaviour
{
    static UiSingleton instance;
    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
    }
}
