﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Player;
    public GameObject Ai;
    public Slider slider;
    public bool aI;
    public Canvas hpSprite;
    public Canvas atkSprite;
    public Image hostile;
    public Image unHostile;
    public Image shooting;
    public Image Unshooting;
    public Image quiver;
    public Text arrowcount;
    void Start()
    {
        

        if (aI)
        {
            //print("test");
            Ai = this.transform.parent.gameObject.transform.parent.gameObject;
           // print(Ai.transform.position);
            hpSprite.sortingLayerName = "Default";
            atkSprite.sortingLayerName = "Default";
        }
        else
        {
            
            Player = GameObject.Find("Player");
            hostile.enabled = false;
            shooting.enabled = false;
            Unshooting.enabled = false;
            quiver.enabled = false;
            arrowcount.enabled = false;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (aI)
        {
            slider.maxValue = Ai.GetComponent<Character>().hp;
            slider.value = Ai.GetComponent<Character>().currenthp;
            if (Ai.GetComponent<Character>().currenthp < Ai.GetComponent<Character>().hp)
            {
                hpSprite.sortingLayerName = "Effects";
            }
            if (Ai.GetComponent<Character>().currenthp == Ai.GetComponent<Character>().hp)
            {
                hpSprite.sortingLayerName = "Default";
            }
            if (Ai.GetComponent<Character>().hostile)
            {
                atkSprite.sortingLayerName = "Effects";
            }
            else
            {
                atkSprite.sortingLayerName = "Default";
            }
        }
        else
        {
            slider.maxValue = Player.GetComponent<Character>().hp;
            slider.value = Player.GetComponent<Character>().currenthp;

            if (Player.GetComponent<Character>().hostile)
            {
                hostile.enabled = true;
                unHostile.enabled = false;
            }
            else
            {
                hostile.enabled = false;
                unHostile.enabled = true;
            }
            if (Player.GetComponent<Character>().weaponvalue == 2)
            {
                Unshooting.enabled = true;
                quiver.enabled = true;
                arrowcount.enabled = true;
                arrowcount.text = (""+Player.GetComponent<Character>().arrowCount);

                if (Player.GetComponent<Character>().shooting)
                {
                    Unshooting.enabled = false;
                    shooting.enabled = true;
                }
                else
                {
                    Unshooting.enabled = true;
                    shooting.enabled = false;
                }
            }
            else
            {
                shooting.enabled = false;
                Unshooting.enabled = false;
                quiver.enabled = false;
                arrowcount.enabled = false;
            }
        }
    }
}
