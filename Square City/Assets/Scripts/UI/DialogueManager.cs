﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialogueManager : MonoBehaviour
{
    public GameObject dialoguePanel;
    public Text dialogue;
    public Text charName;

    public Image CharImage;
    public Image ArmorImage;
    public Image WeaponImage;
    void Start()
    {
        dialoguePanel.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetDialogue(Character Char, string Dialouge)
    {
        dialoguePanel.SetActive(true);
        dialogue.text = Dialouge;
        charName.text = Char.CharName;
        CharImage.sprite = Char.spriteRenderer.sprite;
        ArmorImage.sprite = Char.armorRenderer.sprite;
        WeaponImage.sprite = Char.weaponRenderer.sprite;
        
    }

    public void ClearDialogue()
    {
        dialoguePanel.SetActive(false);
    }
}