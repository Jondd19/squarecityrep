﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{

    public Image controls;
    bool controlImage;
    public GameObject playScreen;
    public GameObject chargenScreen;
    public GameObject arenaChargenScreen;
    public GameObject optionsScreen;
    bool arenaMode;
    bool arenaModeChargen;
    public GameObject Player;
    public Slider bgSlider;
    public Slider efSlider;
    public string bgKey = "bg";
    public string efKey = "ef";
    
    
    public GameObject gameManager;



    public Text infoText;
    public Text arenaInfoText;
    public Image CharImage;
    public Image ArenaCharImage;
    public Image ArenaArmorImage;
    public Image ArenaWeaponImage;
    public Sprite malestone;
    public Sprite femalestone;
    public Sprite malegrug;
    public Sprite femalegrug;
    public Sprite malegnomen;
    public Sprite femalegnomen;
    public Sprite malequick;
    public Sprite femalequick;
    public Sprite ironArmor;
    public Sprite copperArmor;
    public Sprite robesprite;
    public Sprite gnomeIron;
    public Sprite gnomeCopper;
    public Sprite rapier;
    public Sprite swordShield;
    public Sprite bow;
    public Sprite fist;
    public Inventory inventory;
    void Start()
    {
        controls.enabled = false;
        controlImage = false;
        arenaModeChargen = false;
        playScreen.SetActive(false);
        chargenScreen.SetActive(false);
        arenaChargenScreen.SetActive(false);
        optionsScreen.SetActive(false);
        efSlider.value = PlayerPrefs.GetFloat(efKey);
        bgSlider.value = PlayerPrefs.GetFloat(bgKey);
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (controlImage)
            {
                controls.enabled = false;
                controlImage = false;
            }
            if(playScreen.activeSelf == true)
            {
                playScreen.SetActive(false);
            }
            if(chargenScreen.activeSelf == true)
            {
                chargenScreen.SetActive(false);
            }
            if (arenaChargenScreen.activeSelf == true)
            {
                arenaChargenScreen.SetActive(false);
                arenaModeChargen = false;
            }
            if(optionsScreen.activeSelf == true)
            {
                optionsScreen.SetActive(false);
            }
        }
        gameManager.GetComponent<GameManager>().bgSlider.value = bgSlider.value;
        gameManager.GetComponent<GameManager>().efSlider.value = efSlider.value;
    }

    public void OnApplicationQuit()
    {
        PlayerPrefs.SetFloat(bgKey, bgSlider.value);
        PlayerPrefs.SetFloat(efKey, efSlider.value);
    }
    public void Play()
    {
        playScreen.SetActive(true);
    }
    public void SaveLoad()
    {

    }
    public void Controls()
    {
        controls.enabled = true;
        controlImage = true;
    }
    public void Options()
    {
        optionsScreen.SetActive(true);
    }
    public void Quit()
    {

    }

    public void NormalMode()
    {
        chargenScreen.SetActive(true);
        arenaMode = false;
        GrugButton();
    }

    public void ArenaMode()
    {
        chargenScreen.SetActive(true);
        arenaMode = true;
        GrugButton();
    }

    public void GrugButton()
    {
        if (Player.GetComponent<Character>().male)
        {
            CharImage.sprite = malegrug;
        }
        else
        {
            CharImage.sprite = femalegrug;
        }

        Player.GetComponent<Character>().racevalue = 2;

        infoText.text = "Grugs are the strongest, yet dumbest species. Generally fills out the working class roles in society. Live about 70 years.";
    }
    public void GnomeButton()
    {
        if (Player.GetComponent<Character>().male)
        {
            CharImage.sprite = malegnomen;
        }
        else
        {
            CharImage.sprite = femalegnomen;
        }

        Player.GetComponent<Character>().racevalue = 0;

        infoText.text = "Gnomen are the weakest, yet most intelligent species. Most often become managers, merchants, and lawmakers. Live about 100 years. ";
    }
    public void StoneButton()
    {
        if (Player.GetComponent<Character>().male)
        {
            CharImage.sprite = malestone;
        }
        else
        {
            CharImage.sprite = femalestone;
        }

        Player.GetComponent<Character>().racevalue = 1;

        infoText.text = "Stonemen are mostly average in stats, but are a bit stronger and slower than average. The highest in social status, most often nobility and priests. Live about 300 years.";
    }
    public void QuickButton()
    {
        if (Player.GetComponent<Character>().male)
        {
            CharImage.sprite = malequick;
        }
        else
        {
            CharImage.sprite = femalequick;
        }

        Player.GetComponent<Character>().racevalue = 3;

        infoText.text = "Quicklings are the fastest race, but weak in most other things. The bottom tier of society, most often criminals. Live only 20 years.";
    }
    public void MFButton()
    {
        Player.GetComponent<Character>().male = !Player.GetComponent<Character>().male;
        if (Player.GetComponent<Character>().racevalue == 0)
        {
            GnomeButton();
        }
        else if (Player.GetComponent<Character>().racevalue == 1)
        {
            StoneButton();
        }
        else if (Player.GetComponent<Character>().racevalue == 2)
        {
            GrugButton();
        }
        else if (Player.GetComponent<Character>().racevalue == 3)
        {
            QuickButton();
        }
    }

    public void StrongarmButton()
    {

        if (Player.GetComponent<Character>().racevalue == 0 && Player.GetComponent<Character>().male)
        {
            ArenaArmorImage.sprite = gnomeIron;
        }
        else
        {
            ArenaArmorImage.sprite = ironArmor;
        }
        ArenaWeaponImage.sprite = fist;

        Player.GetComponent<Character>().armorvalue = 2;
        Player.GetComponent<Character>().weaponvalue = 3;
        Player.GetComponent<Character>().weaponmatvalue = 0;
        Player.GetComponent<Character>().armored = true;
        Player.GetComponent<Character>().duelist = false;
        Player.GetComponent<Character>().strongarm = true;
        Player.GetComponent<Character>().ranger = false;
        Player.GetComponent<Character>().shieldmaster = false;


        arenaInfoText.text = "Strongarms are unarmed combatants, focused solely on their own physical power. They receive a damage bonus from their armor's quality as long as they wield no weapon.";
    }
    public void ArcherButton()
    {
        if (Player.GetComponent<Character>().racevalue == 0 && Player.GetComponent<Character>().male)
        {
            ArenaArmorImage.sprite = gnomeCopper;
        }
        else
        {
            ArenaArmorImage.sprite = copperArmor;
        }
        ArenaWeaponImage.sprite = bow;

        Player.GetComponent<Character>().armorvalue = 0;
        Player.GetComponent<Character>().weaponvalue = 2;
        Player.GetComponent<Character>().weaponmatvalue = 0;
        Player.GetComponent<Character>().armored = true;
        Player.GetComponent<Character>().duelist = false;
        Player.GetComponent<Character>().strongarm = false;
        Player.GetComponent<Character>().ranger = true;
        Player.GetComponent<Character>().shieldmaster = false;


        arenaInfoText.text = "Rangers are archers who happen to be adept even in close combat. They will deal full weapon damage in melee with a bow, as opposed to the normal strength only damage.";
    }
    public void ShieldmasterButton()
    {
        if (Player.GetComponent<Character>().racevalue == 0 && Player.GetComponent<Character>().male)
        {
            ArenaArmorImage.sprite = gnomeCopper;
        }
        else
        {
            ArenaArmorImage.sprite = copperArmor;
        }
        ArenaWeaponImage.sprite = swordShield;

        Player.GetComponent<Character>().armorvalue = 0;
        Player.GetComponent<Character>().weaponvalue = 1;
        Player.GetComponent<Character>().weaponmatvalue = 0;
        Player.GetComponent<Character>().armored = true;
        Player.GetComponent<Character>().duelist = false;
        Player.GetComponent<Character>().strongarm = false;
        Player.GetComponent<Character>().ranger = false;
        Player.GetComponent<Character>().shieldmaster = true;


        arenaInfoText.text = "Shieldmasters are the defensive specialists of the city. While wielding a sword&shield pair, they gain a bonus to ac depending on the weapon quality.";
    }
    public void DuelistButton()
    {
        if (Player.GetComponent<Character>().racevalue == 0 && Player.GetComponent<Character>().male)
        {
            ArenaArmorImage.sprite = robesprite;
        }
        else
        {
            ArenaArmorImage.sprite = robesprite;
        }
        ArenaWeaponImage.sprite = rapier;

        Player.GetComponent<Character>().armorvalue = 0;
        Player.GetComponent<Character>().weaponvalue = 0;
        Player.GetComponent<Character>().weaponmatvalue = 0;
        Player.GetComponent<Character>().clothesvalue = 1;
        Player.GetComponent<Character>().armored = false;
        Player.GetComponent<Character>().duelist = true;
        Player.GetComponent<Character>().strongarm = false;
        Player.GetComponent<Character>().ranger = false;
        Player.GetComponent<Character>().shieldmaster = false;


        arenaInfoText.text = "Duelists have the highest damage potential of the subguilds, dealing double damage with rapiers as long as they wear no armor.";
    }
    public void ChargenFinished()
    {
        if (arenaMode && !arenaModeChargen)
        {
            arenaModeChargen = true;
            arenaChargenScreen.SetActive(true);
            ArenaCharImage.sprite = CharImage.sprite;
            StrongarmButton();
        }
        else if(arenaMode && arenaModeChargen)
        {
            
            Player.GetComponent<Character>().Statupdate();
            Player.GetComponent<Character>().currenthp = Player.GetComponent<Character>().hp;

            if (Player.GetComponent<Character>().duelist)
            {
                inventory.AddItem(1, 1, 10, "Copper Rapier");
            }
            if (Player.GetComponent<Character>().strongarm)
            {
                inventory.AddItem(2, 1, 10, "Iron Armor");
            }
            if (Player.GetComponent<Character>().ranger)
            {
                inventory.AddItem(1, 1, 10, "Copper Bow");
                inventory.AddItem(2, 1, 10, "Copper Armor");
                inventory.AddItem(3, 10, 10, "Arrow");
            }
            if (Player.GetComponent<Character>().shieldmaster)
            {
                inventory.AddItem(1, 1, 10, "Copper Sword&Shield");
                inventory.AddItem(2, 1, 10, "Copper Armor");
            }


            Player.GetComponent<Character>().armorvalue = 0;
            Player.GetComponent<Character>().weaponvalue = 3;
            Player.GetComponent<Character>().weaponmatvalue = 0;
            Player.GetComponent<Character>().clothesvalue = 0;
            Player.GetComponent<Character>().armored = false;
            

            SceneManager.LoadScene(1);
        }
        else
        {
            
            Player.GetComponent<Character>().Statupdate();
            Player.GetComponent<Character>().currenthp = Player.GetComponent<Character>().hp;
            
            SceneManager.LoadScene(1);
        }
    }
}
