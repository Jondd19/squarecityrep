﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StatsDisplay : MonoBehaviour
{
    public GameObject Player;
    public Text healthText;
    public Text potionText;
    void Start()
    {
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        healthText.text = (Player.GetComponent<Character>().currenthp + "/" + Player.GetComponent<Character>().hp);
        potionText.text = ("" + Player.GetComponent<Character>().potionCount);
    }
}
