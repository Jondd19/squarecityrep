﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct CharacterData
{
    public int ID;
    public int patterns;
    public bool ai;
    


    public bool hostile;
    public bool targeted;
    public int targetingNumber;
    public bool playerTargeted;

    public string CharName;
    public bool male;
    public bool armored;
    public bool duelist;
    public bool strongarm;
    public bool shieldmaster;
    public bool ranger;

    public int racevalue;
    public int clothesvalue;
    public int armorvalue;
    public int weaponvalue;
    public int weaponmatvalue;

    public int hp;
    public int currenthp;
    public int ac;
    public int strength;
    public int agility;
    public int intelligence;
    public int damage;
    public int weaponskill;
    public int gold;
    public int arrowCount;
    public int potionCount;
    public bool alive;

    public float x;
    public float y;
    public float z;
}
[System.Serializable]
public struct InventoryData
{
    public string itemName;
    public int itemCount;
    public int itemPrice;
    public int itemTabValue;
}


[System.Serializable]
public struct QuestData
{

}

[System.Serializable]
public class SaveData
{
    public CharacterData[] CharacterDatas;
   public InventoryData[] inventoryDatas;
    public QuestData questData;
    public int sceneNumber;

    

}
