﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DataGrabber : MonoBehaviour
{
    public SaveData saveData;
    public GameObject[] charList;
    public Inventory inventory;
    public int currentScene;
    //public QuestManager questManager;

    void Start()
    {
        //FindAndGrab();
        currentScene = SceneManager.GetActiveScene().buildIndex;
    }

     void Update()
    {
        /*if (SceneManager.GetActiveScene().buildIndex != currentScene)
        {
            FindAndGrab();
            currentScene = SceneManager.GetActiveScene().buildIndex;
        }*/
    }
    public void SetLoadedValues()
    {


        for (int i = 0; i < saveData.CharacterDatas.Length; i++)
        {
            charList[i].GetComponent<Character>().ID = saveData.CharacterDatas[i].ID;
            charList[i].GetComponent<Character>().patterns = saveData.CharacterDatas[i].patterns;
            charList[i].GetComponent<Character>().ai = saveData.CharacterDatas[i].ai;

            charList[i].GetComponent<Character>().hostile = saveData.CharacterDatas[i].hostile;
            charList[i].GetComponent<Character>().targeted = saveData.CharacterDatas[i].targeted;
            charList[i].GetComponent<Character>().targetingNumber = saveData.CharacterDatas[i].targetingNumber;
            charList[i].GetComponent<Character>().playerTargeted = saveData.CharacterDatas[i].playerTargeted;

            charList[i].GetComponent<Character>().CharName = saveData.CharacterDatas[i].CharName;
            charList[i].GetComponent<Character>().male = saveData.CharacterDatas[i].male;
            charList[i].GetComponent<Character>().armored = saveData.CharacterDatas[i].armored;
            charList[i].GetComponent<Character>().duelist = saveData.CharacterDatas[i].duelist;
            charList[i].GetComponent<Character>().strongarm = saveData.CharacterDatas[i].strongarm;
            charList[i].GetComponent<Character>().shieldmaster = saveData.CharacterDatas[i].shieldmaster;
            charList[i].GetComponent<Character>().ranger = saveData.CharacterDatas[i].ranger;

            charList[i].GetComponent<Character>().racevalue = saveData.CharacterDatas[i].racevalue;
            charList[i].GetComponent<Character>().clothesvalue = saveData.CharacterDatas[i].clothesvalue;
            charList[i].GetComponent<Character>().armorvalue = saveData.CharacterDatas[i].armorvalue;
            charList[i].GetComponent<Character>().weaponvalue = saveData.CharacterDatas[i].weaponvalue;
            charList[i].GetComponent<Character>().weaponmatvalue = saveData.CharacterDatas[i].weaponmatvalue;

            charList[i].GetComponent<Character>().hp = saveData.CharacterDatas[i].hp;
            charList[i].GetComponent<Character>().currenthp = saveData.CharacterDatas[i].currenthp;
            charList[i].GetComponent<Character>().ac = saveData.CharacterDatas[i].ac;
            charList[i].GetComponent<Character>().strength = saveData.CharacterDatas[i].strength;
            charList[i].GetComponent<Character>().agility = saveData.CharacterDatas[i].agility;
            charList[i].GetComponent<Character>().intelligence = saveData.CharacterDatas[i].intelligence;
            charList[i].GetComponent<Character>().damage = saveData.CharacterDatas[i].damage;
            charList[i].GetComponent<Character>().weaponskill = saveData.CharacterDatas[i].weaponskill;
            charList[i].GetComponent<Character>().gold = saveData.CharacterDatas[i].gold;
            charList[i].GetComponent<Character>().arrowCount = saveData.CharacterDatas[i].arrowCount;
            charList[i].GetComponent<Character>().potionCount = saveData.CharacterDatas[i].potionCount;
            charList[i].GetComponent<Character>().alive = saveData.CharacterDatas[i].alive;

            charList[i].transform.position = new Vector3(saveData.CharacterDatas[i].x, saveData.CharacterDatas[i].y, saveData.CharacterDatas[i].z);

        }

        inventory.itemList.Clear();
        inventory.InventoryUpdate();

        for (int i = 0; i < saveData.inventoryDatas.Length; i++)
        {

            inventory.AddItem(saveData.inventoryDatas[i].itemTabValue, saveData.inventoryDatas[i].itemCount, saveData.inventoryDatas[i].itemPrice, saveData.inventoryDatas[i].itemName);


            


        }
        inventory.InventoryUpdate();

    }

    public void FindAndGrab()
    {
        charList = GameObject.FindGameObjectsWithTag("Character");
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
        saveData.sceneNumber = SceneManager.GetActiveScene().buildIndex;
        //questManager = GameObject.Find("QuestManager").GetComponent<questManager>();
        Grab();
    }

    public void Grab()
    {

        List<CharacterData> tempList = new List<CharacterData>();
        for (int i = 0; i < charList.Length; i++)
        {
            CharacterData empty = new CharacterData();
            tempList.Add(empty);
        }
        saveData.CharacterDatas = tempList.ToArray();

            for (int i = 0; i < charList.Length; i++)
        {
            saveData.CharacterDatas[i].ID = charList[i].GetComponent<Character>().ID;
            saveData.CharacterDatas[i].patterns = charList[i].GetComponent<Character>().patterns;
            saveData.CharacterDatas[i].ai = charList[i].GetComponent<Character>().ai;

            saveData.CharacterDatas[i].hostile = charList[i].GetComponent<Character>().hostile;
            saveData.CharacterDatas[i].targeted = charList[i].GetComponent<Character>().targeted;
            saveData.CharacterDatas[i].targetingNumber = charList[i].GetComponent<Character>().targetingNumber;
            saveData.CharacterDatas[i].playerTargeted = charList[i].GetComponent<Character>().playerTargeted;

            saveData.CharacterDatas[i].CharName = charList[i].GetComponent<Character>().CharName;
            saveData.CharacterDatas[i].male = charList[i].GetComponent<Character>().male;
            saveData.CharacterDatas[i].armored = charList[i].GetComponent<Character>().armored;
            saveData.CharacterDatas[i].duelist = charList[i].GetComponent<Character>().duelist;
            saveData.CharacterDatas[i].strongarm = charList[i].GetComponent<Character>().strongarm;
            saveData.CharacterDatas[i].shieldmaster = charList[i].GetComponent<Character>().shieldmaster;
            saveData.CharacterDatas[i].ranger = charList[i].GetComponent<Character>().ranger;

            saveData.CharacterDatas[i].racevalue = charList[i].GetComponent<Character>().racevalue;
            saveData.CharacterDatas[i].clothesvalue = charList[i].GetComponent<Character>().clothesvalue;
            saveData.CharacterDatas[i].armorvalue = charList[i].GetComponent<Character>().armorvalue;
            saveData.CharacterDatas[i].weaponvalue = charList[i].GetComponent<Character>().weaponvalue;
            saveData.CharacterDatas[i].weaponmatvalue = charList[i].GetComponent<Character>().weaponmatvalue;

            saveData.CharacterDatas[i].hp = charList[i].GetComponent<Character>().hp;
            saveData.CharacterDatas[i].currenthp = charList[i].GetComponent<Character>().currenthp;
            saveData.CharacterDatas[i].ac = charList[i].GetComponent<Character>().ac;
            saveData.CharacterDatas[i].strength = charList[i].GetComponent<Character>().strength;
            saveData.CharacterDatas[i].agility = charList[i].GetComponent<Character>().agility;
            saveData.CharacterDatas[i].intelligence = charList[i].GetComponent<Character>().intelligence;
            saveData.CharacterDatas[i].damage = charList[i].GetComponent<Character>().damage;
            saveData.CharacterDatas[i].weaponskill = charList[i].GetComponent<Character>().weaponskill;
            saveData.CharacterDatas[i].gold = charList[i].GetComponent<Character>().gold;
            saveData.CharacterDatas[i].arrowCount = charList[i].GetComponent<Character>().arrowCount;
            saveData.CharacterDatas[i].potionCount = charList[i].GetComponent<Character>().potionCount;
            saveData.CharacterDatas[i].alive = charList[i].GetComponent<Character>().alive;


            saveData.CharacterDatas[i].x = charList[i].transform.position.x;
            saveData.CharacterDatas[i].y = charList[i].transform.position.y;
            saveData.CharacterDatas[i].z = charList[i].transform.position.z;

        }

        List<InventoryData> tempList2 = new List<InventoryData>();
        for (int i = 0; i < inventory.itemList.Count; i++)
        {
            InventoryData empty = new InventoryData();
            tempList2.Add(empty);
        }
        saveData.inventoryDatas = tempList2.ToArray();


        for (int i = 0; i < inventory.itemList.Count; i++)
        {
            saveData.inventoryDatas[i].itemName = inventory.itemArray[i].itemName;
            saveData.inventoryDatas[i].itemCount = inventory.itemArray[i].invCount;
            saveData.inventoryDatas[i].itemPrice = inventory.itemArray[i].price;
            saveData.inventoryDatas[i].itemTabValue = inventory.itemArray[i].tabValue;
        }

        


    }
}
