﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
public class SaveManager : MonoBehaviour
{
    public DataGrabber dataGrabber;
    static SaveManager instance;



    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }



    }

    public void Update()
    {
        
    }

    public void Save(string filename)
    {

        dataGrabber.FindAndGrab();

       //Create a file or open a file to save to.
       FileStream file = new FileStream(Application.persistentDataPath + "/"+filename+".dat", FileMode.OpenOrCreate);
        //Binary Formatter: Allows us to write data to a file
        BinaryFormatter formatter = new BinaryFormatter();
        //Serialize method to write to the file
        formatter.Serialize(file, dataGrabber.saveData);
        file.Close();
    }


    public void Load(string filename)
    {
        FileStream file = new FileStream(Application.persistentDataPath + "/" + filename + ".dat", FileMode.Open);
        BinaryFormatter formatter = new BinaryFormatter();
        dataGrabber.saveData = (SaveData) formatter.Deserialize(file);
        file.Close();

        
            dataGrabber.SetLoadedValues();
        
    }
}
