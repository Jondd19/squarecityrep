﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class Character : MonoBehaviour
    {

    static Character playerInstance;
    
    public int ID;
    public bool unique = true;
        public int patterns;
        public bool ai;
        public int direction;
        
        
    public bool hostile;
    public bool targeted;
    public int targetingNumber;
    public bool playerTargeted;

        public string CharName;
        public bool male;
        public bool armored;
        public bool duelist;
        public bool strongarm;
        public bool shieldmaster;
        public bool ranger;

    public GameObject target;
    Vector3[] path;
    

    public int racevalue;
        public int clothesvalue;
        public int armorvalue;
        public int weaponvalue;
        public int weaponmatvalue;

        public int hp;
    public int currenthp;
        public int ac;
        public int strength;
        public int agility;
        public int intelligence;
        public int damage;
        public int weaponskill;
        public int gold;
    public int arrowCount;
    public int potionCount;
    public bool alive;

    public    Vector3 left;
    public   Vector3 right;
    public    Vector3 up;
    public   Vector3 down;
    public Vector3 lastPos;

    public GameObject Player;
    public GameObject goldToSpawn;
    public GameObject indicatorToSpawn;
    GameObject indicator;
    GameObject goldSpawn;
    public GameObject gameManager;
    public Inventory inventory;

    public DialogueManager dialogueManager;

    public AudioSource soundEffects;
    
    public AudioClip swordSound;
    public AudioClip shieldSound;
    public AudioClip punchSound;
    public AudioClip arrowSound;
    public AudioClip menuBG;
    public AudioClip gameBG;

    public SpriteRenderer spriteRenderer;
        public SpriteRenderer armorRenderer;
    public SpriteRenderer weaponRenderer;
        public Sprite malestone;
        public Sprite femalestone;
        public Sprite malegrug;
        public Sprite femalegrug;
        public Sprite malegnomen;
        public Sprite femalegnomen;
        public Sprite malequick;
        public Sprite femalequick;

    public Sprite CopperArmor;
    public Sprite BronzeArmor;
    public Sprite IronArmor;
    public Sprite SteelArmor;
    public Sprite ForceSteelArmor;
    public Sprite BlueSteelArmor;
    public Sprite LightSteelArmor;

    public Sprite GnomeCopperArmor;
    public Sprite GnomeBronzeArmor;
    public Sprite GnomeIronArmor;
    public Sprite GnomeSteelArmor;
    public Sprite GnomeForceSteelArmor;
    public Sprite GnomeBlueSteelArmor;
    public Sprite GnomeLightSteelArmor;

    public Sprite Silk;
    public Sprite BlueSilk;
    public Sprite ForceSilk;
    public Sprite LightSilk;

    public Sprite GnomeSilk;
    public Sprite GnomeBlueSilk;
    public Sprite GnomeForceSilk;
    public Sprite GnomeLightSilk;

    public Sprite rapier;
    public Sprite fist;
    public Sprite bow;
    public Sprite shield;


    public Collider2D boxCollider;
    public Canvas AIHPCanvas;
    public Canvas AIHostileCanvas;
    //archery stuff below here
    GameObject[] targets;
    int targetCount = 0;
    int maxCount;
    public bool shooting = false;
    public enum Race
        {
            Gnomeman,
            Stoneman,
            Grug,
            Quickling
        };

        public enum Armor
        {
            Copper,
            Bronze,
            Iron,
            Steel,
            Lightsteel,
            Forcesteel,
            Bluesteel
        };

        public enum Clothes
        {
            Cotton,
            Silk,
            Lightsilk,
            Forcesilk,
            Bluesilk
        };

        public enum Weapon
        {
            Sword,
            Shield,
            Bow,
            Unarmed
        };

        public enum Weaponmat
        {
            Copper,
            Bronze,
            Iron,
            Steel,
            Lightsteel,
            Forcesteel,
            Bluesteel
        };

        public enum Patterns
        {
            Stay,
            Wander,
            Seek
        };

    void Awake()
    {
        gameManager = GameObject.Find("GameManager");

        foreach (GameObject Char in gameManager.GetComponent<GameManager>().charList)
        {
            if (Char.GetComponent<Character>().ID == ID)
            {
                Destroy(this.gameObject);
            }
        }

        
            DontDestroyOnLoad(this);
        
    }
    void Start()
    {
        alive = true;
        Statupdate();
        StartupPos();
        currenthp = hp;
        //Target(target);
        targetingNumber = 0;
        soundEffects = this.gameObject.GetComponent<AudioSource>();
        dialogueManager = GameObject.Find("UICanvas").GetComponent<DialogueManager>();

        if (ai)
        {
            Player = GameObject.Find("Player");
            Target(Player);
        }
        else
        {
            Player = this.gameObject;
            
        }
        
        gameManager = GameObject.Find("GameManager");
       
        if (!ai)
        {
           // Shoot();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
        if (!alive && ai)
        {
            spriteRenderer.enabled = false;
            armorRenderer.enabled = false;
            weaponRenderer.enabled = false;
            boxCollider.enabled = false;
            AIHPCanvas.enabled = false;
            AIHostileCanvas.enabled = false;
        }
        else if (ai)
        {
            spriteRenderer.enabled = true;
            armorRenderer.enabled = true;
            weaponRenderer.enabled = true;
            boxCollider.enabled = true;
            AIHPCanvas.enabled = true;
            AIHostileCanvas.enabled = true;
        }


        if (currenthp > hp)
        {
            currenthp = hp;
        }


        if (currenthp <= 0 && alive)
        {
            if (ai)
            {
                if (target != null)
                {

                    if (target.GetComponent<Character>().targetingNumber != 0)
                    {
                        target.GetComponent<Character>().targetingNumber--;
                    }
                    else
                    {
                        target.GetComponent<Character>().targeted = false;
                    }

                    if (target.GetComponent<Character>().playerTargeted)
                    {
                        target.GetComponent<Character>().playerTargeted = false;
                    }

                }
                goldSpawn = Instantiate(goldToSpawn, transform.position, Quaternion.identity);
                goldSpawn.GetComponent<Gold>().gold += gold;

                alive = false;
            }
        }



        if (shooting == true)
        {
            
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (targetCount == maxCount-1)
                {
                    targetCount = 0;
                }
                else if( maxCount>=2)
                {
                    targetCount++;
                }
                indicator.transform.position = targets[targetCount].transform.position;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (targetCount == 0)
                {
                    targetCount = maxCount-1;
                }
                 else if (maxCount >= 2)
                {
                    targetCount--;
                }
                indicator.transform.position = targets[targetCount].transform.position;
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
               

                int roll = Random.Range(1, 11);
                roll += weaponskill;
                //soundEffects.clip = arrowSound;
                soundEffects.PlayOneShot(arrowSound);
                soundEffects.time = 0.7f;
                if (roll > targets[targetCount].GetComponent<Character>().ac)
                {
                    
                        targets[targetCount].GetComponent<Character>().currenthp -= damage;
                    
                }

                targets[targetCount].GetComponent<Character>().hostile = true;
                targets[targetCount].GetComponent<Character>().patterns = 2;
                targets[targetCount].GetComponent<Character>().Target(this.gameObject);

                Destroy(indicator);
                shooting = false;
                Player.GetComponent<PlayerMovement>().moveMode = true;
                arrowCount -= 1;
                inventory.RemoveItem(1, "Arrow");
                gameManager.GetComponent<GameManager>().Tick();
            }

        }


        var currentpos = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), Mathf.Round(transform.position.z));
        
    }


        void OnCollisionEnter2D(Collision2D collision)
        {
            //collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;

            //print("test");

            //Gridsnap();
        }

        void OnCollisionExit2D(Collision2D collision)
        {

        if (hostile)
        {
            int roll = Random.Range(1, 10);
            roll += weaponskill;

            if(weaponvalue == 0)
            {
                soundEffects.clip = swordSound;
                soundEffects.Play();
                soundEffects.time = 0.5f;
            }
            if (weaponvalue == 1)
            {
                soundEffects.clip = shieldSound;
                soundEffects.Play();
                soundEffects.time = 0.6f;
            }
            if (weaponvalue == 2)
            {
                soundEffects.clip = punchSound;
                soundEffects.Play();
                soundEffects.time = 0.6f;
            }
            if (weaponvalue == 3)
            {
                soundEffects.clip = punchSound;
                soundEffects.Play();
                soundEffects.time = 0.6f;
            }

            if (collision.gameObject.GetComponent<Character>() != null)
            {
                if (roll > collision.gameObject.GetComponent<Character>().ac)
                {
                    if (!ranger && weaponvalue == 2)
                    {
                        collision.gameObject.GetComponent<Character>().currenthp -= strength;
                    }
                    else
                    {
                        collision.gameObject.GetComponent<Character>().currenthp -= damage;
                    }
                }

                collision.gameObject.GetComponent<Character>().hostile = true;
                collision.gameObject.GetComponent<Character>().patterns = 2;
                Target(collision.gameObject);
                collision.gameObject.GetComponent<Character>().Target(this.gameObject);
            }
            if(ai)
            {
                if(collision.gameObject.GetComponent<Character>().currenthp <= 0)
                {
                    collision.otherCollider.gameObject.GetComponent<Character>().hostile = false;
                    collision.otherCollider.gameObject.GetComponent<Character>().patterns = 1;
                }
            }
        }
        else if (ai && Player.GetComponent<Character>().hostile == false)
        {
            print("test");
            int droll = Random.Range(1, 4);



            if (droll == 1)
            {
                dialogueManager.SetDialogue(this, "Pardon me.");
            }
            else if (droll == 2)
            {
                dialogueManager.SetDialogue(this, "Hello there!");
            }
            else if (droll == 3)
            {
                dialogueManager.SetDialogue(this, "Outta the way, Dumbass.");
            }
            else if (droll == 4)
            {
                dialogueManager.SetDialogue(this, "Are you there, god? Its me Fioooona!");
            }

             if (collision.otherCollider.gameObject.GetComponent<Character>().racevalue == 2)
            {
                dialogueManager.SetDialogue(this, "Grug no like you.");
            }
        }

        

        }


        public void Gridsnap()
        {
            var currentpos = new Vector3(Mathf.Round(transform.position.x), Mathf.Round(transform.position.y), Mathf.Round(transform.position.z));
            transform.position = currentpos;
        }

        public void Wander()
        {
        if (target != null)
        {

            if (target.GetComponent<Character>().targetingNumber != 0)
            {
                target.GetComponent<Character>().targetingNumber--;
            }
            else
            {
                target.GetComponent<Character>().targeted = false;
            }

            if (target.GetComponent<Character>().playerTargeted)
            {
                target.GetComponent<Character>().playerTargeted = false;
            }

        }
        direction = Random.Range(0, 3);

            Move(direction);
        }

    public void Stay()
    {
        if (target != null)
        {

            if (target.GetComponent<Character>().targetingNumber != 0)
            {
                target.GetComponent<Character>().targetingNumber--;
            }
            else
            {
                target.GetComponent<Character>().targeted = false;
            }

            if (target.GetComponent<Character>().playerTargeted)
            {
                target.GetComponent<Character>().playerTargeted = false;
            }

        }
        target = null;
    }
    public void Seek()
    {

        
        PathRequestManager.RequestPath(transform.position, target.transform.position, OnPathFound);
        Target(target);

    }

    IEnumerator SeekMove()
    {
        //print(path[0]);
        
        if (path[0].y == this.transform.position.y && path[0].x < this.transform.position.x)
        {
            Move(0);
            yield break;
        }
        else if (path[0].y == transform.position.y && path[0].x > transform.position.x)
        {
            Move(1);
            yield break;
        }
        else if (path[0].x == transform.position.x && path[0].y > transform.position.y)
        {
            Move(2);
            yield break;
        }
        else if (path[0].x == transform.position.x && path[0].y < transform.position.y)
        {
            Move(3);
            yield break;
        }

        yield return null;
    }

    public void Target(GameObject targeted)
    {
        if(target != null)
        {
            
            if (target.GetComponent<Character>().targetingNumber != 0)
            {
                target.GetComponent<Character>().targetingNumber--;
            }
            else
            {
                target.GetComponent<Character>().targeted = false;
            }
            
            if (target.GetComponent<Character>().playerTargeted)
            {
                target.GetComponent<Character>().playerTargeted = false;
            }

        }
        target = targeted;
       


        if (!ai)
        {
            target.GetComponent<Character>().playerTargeted = true;
        }
        else
        {
            target.GetComponent<Character>().targeted = true;
            target.GetComponent<Character>().targetingNumber++;
        }
    }

    public void Shoot()
    {
        
        List<GameObject> potentialTargets = new List<GameObject>();
        LayerMask mask = LayerMask.GetMask("Wall");
        targetCount = 0;
        foreach (GameObject Char in gameManager.GetComponent<GameManager>().charList)
        {

            //print("test");
            if (Char != null && Char.GetComponent<Character>().alive)
            {
                if (Vector3.Distance(Char.transform.position, transform.position) <= 4)
                {
                    if (Physics2D.Linecast(transform.position, Char.transform.position, mask))
                    {
                        //print("blocked");

                    }
                    else
                    {
                        if (Char != this.gameObject)
                            potentialTargets.Add(Char);
                        // print("added");
                    }

                }
            }
        }
         targets = potentialTargets.ToArray();
        if (ai)
        {
            foreach(GameObject targetez in targets)
            {
                if (targetez != null && targetez.GetComponent<Character>().alive)
                {
                    if (targetez == target)
                    {
                        int roll = Random.Range(1, 10);
                        roll += weaponskill;
                        soundEffects.clip = arrowSound;
                        soundEffects.Play();
                        soundEffects.time = 0.8f;
                        if (roll > targetez.GetComponent<Character>().ac)
                        {
                           
                                targetez.GetComponent<Character>().currenthp -= damage;
                            
                        }

                        targetez.GetComponent<Character>().hostile = true;
                        targetez.GetComponent<Character>().patterns = 2;
                        targetez.GetComponent<Character>().Target(this.gameObject);
                        arrowCount -= 1;
                    }
                }
            }
        }
        else
        {
            if (targets.Length != 0)
            {
                Player.GetComponent<PlayerMovement>().moveMode = false;
                indicator = Instantiate(indicatorToSpawn, targets[0].transform.position, Quaternion.identity);

                maxCount = targets.Length;
                shooting = true;
            }
        }
    }
    public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {
        
        if (pathSuccessful)
       {
            
            path = newPath;
            StopCoroutine("SeekMove");
            StartCoroutine("SeekMove");
        }
    }
        void OnCollisionStay2D(Collision2D collision)
        {
            
                if (direction == 0)
                {
               
                this.transform.position = left;
                
                
            }
                else if (direction == 1)
                {
                    this.transform.position = right;
                
            }
                else if (direction == 2)
                {
                    this.transform.position = up;
               
            }
                else if (direction == 3)
                {
                    this.transform.position = down;
                
            }
        else
        {
            Gridsnap();
        }
                
                    Gridsnap();
                
            

            
        }

        public void Move(int x)
        {
        direction = x;
        lastPos = transform.position;
            if (x == 0)
            {
          
                left = transform.position;
            
                this.transform.Translate(-1, 0, 0);
                
                
            }

            if (x == 1)
            {
                right = transform.position;
            
                this.transform.Translate(1, 0, 0);
                

            }
            if (x == 2)
            {
                up = transform.position;
                this.transform.Translate(0, 1, 0);
                
            }
            if (x == 3)
            {
                down = transform.position;
                this.transform.Translate(0, -1, 0);
                

            }
        }

        public void StartupPos()
        {
            left = transform.position;
            right = transform.position;
            up = transform.position;
            down = transform.position;
        }
        public void Statupdate()
        {
        if (!ai)
        {
            if (inventory.itemArray != null)
            {
                bool arrows = false;
                bool potions = false;
                foreach (InventoryItem item in inventory.itemArray)
                {
                    if (item.itemName == "Arrow")
                    {
                        
                        arrowCount = item.invCount;
                        arrows = true;
                    }
                    if (item.itemName == "Health Potion")
                    {
                        potionCount = item.invCount;
                        potions = true;
                    }
                }
                if(arrows == false)
                {
                    arrowCount = 0;
                }
                if(potions == false)
                {
                    potionCount = 0;
                }
            }
        }
            if (male)
            {
                if (racevalue == 0)
                {
                    strength = 2;
                    intelligence = 5;
                    agility = 2;
                    spriteRenderer.sprite = malegnomen;
                }
                else if (racevalue == 1)
                {
                    strength = 4;
                    intelligence = 4;
                    agility = 1;
                    spriteRenderer.sprite = malestone;
                }
                else if (racevalue == 2)
                {
                    strength = 5;
                    intelligence = 1;
                    agility = 3;
                    spriteRenderer.sprite = malegrug;
                }
                else if (racevalue == 3)
                {
                    strength = 2;
                    intelligence = 2;
                    agility = 5;
                    spriteRenderer.sprite = malequick;
                }
            }
            else
            {
                if (racevalue == 0)
                {
                    strength = 2;
                    intelligence = 5;
                    agility = 2;
                    spriteRenderer.sprite = femalegnomen;
                }
                else if (racevalue == 1)
                {
                    strength = 4;
                    intelligence = 4;
                    agility = 1;
                    spriteRenderer.sprite = femalestone;
                }
                else if (racevalue == 2)
                {
                    strength = 5;
                    intelligence = 1;
                    agility = 3;
                    spriteRenderer.sprite = femalegrug;
                }
                else if (racevalue == 3)
                {
                    strength = 2;
                    intelligence = 2;
                    agility = 5;
                    spriteRenderer.sprite = femalequick;
                }
            }




            hp = strength * 5;
            ac = agility;
            damage = strength;

            if (weaponvalue == 0)
            {
            weaponRenderer.sprite = rapier;
                if (weaponmatvalue == 0)
                {
                    damage += 1;
                }
                else if (weaponmatvalue == 1)
                {
                    damage += 2;
                }
                else if (weaponmatvalue == 2)
                {
                    damage += 3;
                }
                else if (weaponmatvalue == 3)
                {
                    damage += 4;
                }
                else if (weaponmatvalue == 4)
                {
                    damage += 4;
                    ac += 1;
                    agility += 1;
                }
                else if (weaponmatvalue == 5)
                {
                    damage += 5;
                    hp += 5;
                    strength += 1;
                }
                else if (weaponmatvalue == 6)
                {
                    damage += 4;
                    intelligence += 1;
                }
            }
            else if (weaponvalue == 1)
            {
            weaponRenderer.sprite = shield;
                if (weaponmatvalue == 0)
                {
                    damage += 1;
                    if (shieldmaster)
                    {
                        ac += 1;
                    }
                }
                else if (weaponmatvalue == 1)
                {
                    damage += 2;
                    if (shieldmaster)
                    {
                        ac += 1;
                    }
                }
                else if (weaponmatvalue == 2)
                {
                    damage += 3;
                    if (shieldmaster)
                    {
                        ac += 2;
                    }
                }
                else if (weaponmatvalue == 3)
                {
                    damage += 4;
                    if (shieldmaster)
                    {
                        ac += 2;
                    }
                }
                else if (weaponmatvalue == 4)
                {
                    damage += 4;
                    ac += 1;
                    agility += 1;
                    if (shieldmaster)
                    {
                        ac += 2;
                    }
                }
                else if (weaponmatvalue == 5)
                {
                    damage += 5;
                    hp += 5;
                    strength += 1;
                    if (shieldmaster)
                    {
                        ac += 2;
                    }
                }
                else if (weaponmatvalue == 6)
                {
                    damage += 4;
                    intelligence += 1;
                    if (shieldmaster)
                    {
                        ac += 2;
                    }
                }
            }
            else if (weaponvalue == 2)
            {
            weaponRenderer.sprite = bow;
                if (weaponmatvalue == 0)
                {
                    damage += 1;
                }
                else if (weaponmatvalue == 1)
                {
                    damage += 2;
                }
                else if (weaponmatvalue == 2)
                {
                    damage += 3;
                }
                else if (weaponmatvalue == 3)
                {
                    damage += 4;
                }
                else if (weaponmatvalue == 4)
                {
                    damage += 4;
                    ac += 1;
                    agility += 1;
                }
                else if (weaponmatvalue == 5)
                {
                    damage += 5;
                    hp += 5;
                    strength += 1;
                }
                else if (weaponmatvalue == 6)
                {
                    damage += 4;
                    intelligence += 1;
                }
            }
            else if (weaponvalue == 3)
            {
            //unarmed
            weaponRenderer.sprite = fist;
            }

            if (armored)
            {
                if (armorvalue == 0)
                {
                    ac += 1;
                if(racevalue == 0 && male)
                {
                    armorRenderer.sprite = GnomeCopperArmor;
                }
                armorRenderer.sprite = CopperArmor;
                    if (strongarm)
                    {
                        damage += 1;
                    }

                }
                if (armorvalue == 1)
                {
                    ac += 2;
                if (racevalue == 0 && male)
                {
                    armorRenderer.sprite = GnomeBronzeArmor;
                }
                armorRenderer.sprite = BronzeArmor;
                if (strongarm)
                    {
                        damage += 2;
                    }
                }
                if (armorvalue == 2)
                {
                    ac += 3;
                if (racevalue == 0 && male)
                {
                    armorRenderer.sprite = GnomeIronArmor;
                }
                armorRenderer.sprite = IronArmor;
                if (strongarm)
                    {
                        damage += 3;
                    }
                }
                if (armorvalue == 3)
                {
                    ac += 4;
                if (racevalue == 0 && male)
                {
                    armorRenderer.sprite = GnomeSteelArmor;
                }
                armorRenderer.sprite = SteelArmor;
                if (strongarm)
                    {
                        damage += 4;
                    }
                }
                if (armorvalue == 4)
                {
                    ac += 5;
                    agility += 1;
                if (racevalue == 0 && male)
                {
                    armorRenderer.sprite = GnomeLightSteelArmor;
                }
                armorRenderer.sprite = LightSteelArmor;
                if (strongarm)
                    {
                        damage += 4;
                    }
                }
                if (armorvalue == 5)
                {
                    ac += 4;
                    damage += 1;
                    hp += 5;
                    strength += 1;
                if (racevalue == 0 && male)
                {
                    armorRenderer.sprite = GnomeForceSteelArmor;
                }
                armorRenderer.sprite = ForceSteelArmor;
                if (strongarm)
                    {
                        damage += 4;
                    }
                }
                if (armorvalue == 6)
                {
                    ac += 4;
                    intelligence += 1;
                if (racevalue == 0 && male)
                {
                    armorRenderer.sprite = GnomeBlueSteelArmor;
                }
                armorRenderer.sprite = BlueSteelArmor;
                if (strongarm)
                    {
                        damage += 4;
                    }
                }
            }
            else
            {
                if (clothesvalue == 0)
                {
                armorRenderer.sprite = null;
                if (weaponvalue == 1 && duelist)
                    {
                        damage *= 2;
                    }
                }
                if (clothesvalue == 1)
                {
                    ac += 1;
                armorRenderer.sprite = Silk;
                if (weaponvalue == 1 && duelist)
                    {
                        damage *= 2;
                    }
                }
                if (clothesvalue == 2)
                {
                    ac += 3;
                    agility += 2;
                armorRenderer.sprite = LightSilk;
                if (weaponvalue == 1 && duelist)
                    {
                        damage *= 2;
                    }
                }
                if (clothesvalue == 3)
                {
                    ac += 1;
                    hp += 10;
                    damage += 2;
                    strength += 2;
                armorRenderer.sprite = ForceSilk;
                if (weaponvalue == 1 && duelist)
                    {
                        damage *= 2;
                    }
                }
                if (clothesvalue == 4)
                {
                    ac += 1;
                    intelligence += 2;
                    if (weaponvalue == 1 && duelist)
                    {
                        damage *= 2;
                    }
                }
            }
        }
    }
