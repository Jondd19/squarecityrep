﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
    {
    

    static GameManager instance;

        public GameObject[] charList;
    public List<IDClass> idList;
        public GameObject Player;
    public GameObject Astar;



    public string bgKey = "bg";
    public string efKey = "ef";
    public float bgVolume;
    public float efVolume;
    public GameObject optionsMenu;
    public Slider bgSlider;
    public Slider efSlider;

    public Canvas uiCan;
    public Canvas pauseCan;
    public bool paused;
    public Image controls;
    bool controlImage;
    public Image inventorybg;
    bool inventoryImage;
    public GameObject inventoryMenu;
    public Inventory inventory;
    public bool isShopping = false;
    public Griddle grid;
    public SaveManager saveManager;
    public DialogueManager dialogueManager;
    public int currentScene;


    float countDown = .1f;
    float Stopwatch;
    bool isOverlap;
    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        currentScene = SceneManager.GetActiveScene().buildIndex;
    }


    void Start()
        {
        bgVolume = 1f;
        efVolume = 1f;
        idList = new List<IDClass>();
        CharListFind();
         Player = GameObject.Find("Player");
         Astar = GameObject.Find("A_");
        dialogueManager = GameObject.Find("UICanvas").GetComponent<DialogueManager>();
        pauseCan.enabled = false;
        controls.enabled = false;
        inventorybg.enabled = false;
        controlImage = false;
        inventoryImage = false;
        inventoryMenu.SetActive(false);
        optionsMenu.SetActive(false);
        
    }

    void CharListFind()
    {
        
        charList = GameObject.FindGameObjectsWithTag("Character");
        

    }
        // Update is called once per frame
        void Update()
        {
        CharListFind();

        resetPos();

        if (isOverlap == true)
        {
            Stopwatch += Time.deltaTime;
            if (Stopwatch >= countDown)
            {
                isOverlap = false;
                resetPos();
                if (isOverlap) {    
                    if (Player.GetComponent<Character>().direction == 0)
                {
                        Player.transform.position = Player.transform.position + Vector3.left;
                }
                if (Player.GetComponent<Character>().direction == 1)
                {
                        Player.transform.position = Player.transform.position + Vector3.right;
                    }
                if (Player.GetComponent<Character>().direction == 2)
                {
                        Player.transform.position = Player.transform.position + Vector3.up;
                    }
                if (Player.GetComponent<Character>().direction == 3)
                {
                        Player.transform.position = Player.transform.position + Vector3.down;
                    }
                    isOverlap = false;
            }
                
                
            }
        }
        else
        {
            Stopwatch = 0;
        }

        this.gameObject.GetComponent<AudioSource>().volume = bgSlider.value;
        
        foreach (GameObject Char in charList)
        {
            if (Char.GetComponent<AudioSource>() != null)
            {
                Char.GetComponent<AudioSource>().volume = efSlider.value;
            }


            

            }
        



        if (currentScene!= SceneManager.GetActiveScene().buildIndex)
        {
            CharListFind();
            currentScene = SceneManager.GetActiveScene().buildIndex;
            grid.ReAwake();
            grid.gameObject.GetComponent<PathFinding>().ReAwake();
            grid.gameObject.GetComponent<PathRequestManager>().ReAwake();
            Tick();
            dialogueManager.SetDialogue(Player.GetComponent<Character>(), "I should equip my items!");
            Player.GetComponent<PlayerMovement>().moveMode = true;
        }

        if ( SceneManager.GetActiveScene().buildIndex == 0)
        {
            Player.GetComponent<PlayerMovement>().moveMode = false;
        }
        


        if (!paused)
        {
            pauseCan.enabled = false;


            if (Input.GetKeyDown(KeyCode.V))
            {
                saveManager.Save("fullGameData");
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                saveManager.Load("fullGameData");
            }


            if (Input.GetKeyDown(KeyCode.S))
            {
                if (!isShopping)
                {
                    
                    SceneManager.LoadScene(3);
                    isShopping = true;
                    Player.GetComponent<PlayerMovement>().moveMode = false;
                    grid.ReAwake();
}
                else
                {
                    SceneManager.LoadScene(1);
                    isShopping = false;
                    Player.GetComponent<PlayerMovement>().moveMode = true;
                    grid.ReAwake();
                }
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Tick();
            }
            if (Input.GetKeyDown(KeyCode.H))
            {
                Player.GetComponent<Character>().hostile = !Player.GetComponent<Character>().hostile;
            }
            if (Input.GetKeyDown(KeyCode.T) && Player.GetComponent<Character>().weaponvalue == 2 && Player.GetComponent<Character>().arrowCount > 0 && Player.GetComponent<Character>().shooting == false && Player.GetComponent<Character>().hostile == true)
            {
                //Player.GetComponent<PlayerMovement>().moveMode = false;

                Player.GetComponent<Character>().Shoot();
                //Tick();
            }
            if (Input.GetKeyDown(KeyCode.P))
            {

                bool potions = false;
                foreach (InventoryItem item in inventory.itemArray)
                {
                    
                    if (item.itemName == "Health Potion")
                    {
                        
                        potions = true;
                    }
                }

                if (potions == true)
                {
                 inventory.RemoveItem(1, "Health Potion");
                Player.GetComponent<Character>().currenthp += 5;
                Player.GetComponent<Character>().Statupdate();
                }
                
            }

            if (Input.GetKeyDown(KeyCode.Escape) && isShopping==false)
            {
               
                paused = true;
                Player.GetComponent<PlayerMovement>().moveMode = false;
                //uiCan.enabled = false;
            }
        }
        else
        {
            pauseCan.enabled = true;
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (controlImage)
                {
                    controls.enabled = false;
                    controlImage = false;
                }
                else if (inventoryImage)
                {
                    inventorybg.enabled = false;
                    inventoryImage = false;
                    inventoryMenu.SetActive(false);
                }
                else if (optionsMenu.activeSelf == true)
                {
                    optionsMenu.SetActive(false);
                }
                else
                {
                    paused = false;
                    Player.GetComponent<PlayerMovement>().moveMode = true;
                   // uiCan.enabled = true;
                }
                
            }
        }
    }


    public void resetPos()
    {
         foreach (GameObject Char in charList)
        {
            


            foreach (GameObject Char2 in charList)
            {
                if (Char.transform.position == Char2.transform.position && Char != Char2 && Char==Player)
                {
                    isOverlap = true;
                }
            }

            }
    }

    public void OnApplicationQuit()
    {
        PlayerPrefs.SetFloat(bgKey, bgSlider.value);
        PlayerPrefs.SetFloat(efKey, efSlider.value);
    }

    public void UnPause()
    {
        
            pauseCan.enabled = true;
            
                paused = false;
                Player.GetComponent<PlayerMovement>().moveMode = true;
                //uiCan.enabled = true;
            
        
    }

    public void QuestsShow()
    {

    }
    public void InventoryShow()
    {
        inventorybg.enabled = true;
        inventoryImage = true;
        inventoryMenu.SetActive(true);
        inventory.AllSwap();
    }

    public void ControlsShow()
    {
        controls.enabled = true;
        controlImage = true;
    }

    public void OptionsShow()
    {
        optionsMenu.SetActive(true);
    }
    public void Quit()
    {
        
    }
    public void Tick()
        {

        
        Player.GetComponent<Character>().Gridsnap();
        
        Astar.GetComponent<Griddle>().CreateGrid();
        foreach (GameObject Char in charList)
        {
            Char.GetComponent<Character>().Statupdate();

               

                



                if (Char != null && Char.GetComponent<Character>().alive)
            {
                
                    Char.GetComponent<Character>().Gridsnap();
                if (Char.GetComponent<Character>().patterns == 0 && Char.GetComponent<Character>().ai == true)
                {

                    Char.GetComponent<Character>().Stay();
                    Char.GetComponent<Character>().Gridsnap();
                }
                if (Char.GetComponent<Character>().patterns == 1 && Char.GetComponent<Character>().ai == true)
                {

                    Char.GetComponent<Character>().Wander();
                    Char.GetComponent<Character>().Gridsnap();
                }
                if (Char.GetComponent<Character>().patterns == 2 && Char.GetComponent<Character>().ai == true && Char.GetComponent<Character>().target!=null)
                {
                    if (Vector3.Distance(Char.GetComponent<Character>().target.transform.position, Char.GetComponent<Character>().transform.position) <= 4 && Char.GetComponent<Character>().arrowCount > 0 && Char.GetComponent<Character>().weaponvalue == 2 && Char.GetComponent<Character>().hostile == true)
                    {
                        Char.GetComponent<Character>().Shoot();
                    }
                    else
                    {
                        Char.GetComponent<Character>().Seek();
                        //print("test");
                    }
                    Char.GetComponent<Character>().Gridsnap();
                }
            }
        }

        }
    }

