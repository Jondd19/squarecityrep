﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDClass 
{
   public int ID;
   public bool unique;

    public IDClass(int id, bool Unique)
    {
        ID = id;
        unique = Unique;
    }


}
